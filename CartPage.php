<?php
include_once 'HomePage.php';
$homeobj = new HomePage();
$ProductCategorydata = $homeobj->GetSelectedCartDetails(1);
$cartCount = $homeobj->GetProductCartDetails('1');
if (is_array($cartCount) && isset($cartCount[0])) {
    $cartCount = $cartCount[0];
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Shopping Demo</title>
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/font-awesome.min.css" rel="stylesheet"/>  
        <link href="assets/ItemSlider/css/main-style.css" rel="stylesheet"/>
        <link href="assets/css/style.css" rel="stylesheet"/>
        <link href="assets/css/my.css" rel="stylesheet"/>
    </head>
    <body>
        <form action="CheckOut.php" method="POST" id="addForm" >
            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><strong>Shopping Demo</strong></a>
                </div>
                <a class="btn btn-large" href="index.php" style="margin-left: 800px;margin-top: 10px;background-color: white;color: black;"><strong>Back</strong></a>
            </nav>

            <?php
            $totalPrice = 0;
            $totalquantity = 0;
            if (is_array($ProductCategorydata) && !empty($ProductCategorydata)) {
                foreach ($ProductCategorydata as $ProductData) {
                    $totalPrice = $totalPrice + $ProductData['PurchsedPrice'];
                    $totalquantity = $totalquantity + 1;
                    ?>
                    <div class="container">
                        <div class='col-md-3 text-center col-sm-6 col-xs-6'>
                            <div class='thumbnail product-box' style="height: 120px;width: 1200px;" >
                                <table>
                                    <tr>
                                        <td><img src='assets/img/dummyimg.png' style="height: 80px;width: 100px;"/></td>
                                        <td><h5><a><font size="5"><?php echo '<b>' . $ProductData['ProductCategoryName'] . '</b>'; ?></font></a></h5></td>
                                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                        <td><h5><a><font size="4">&nbsp;&nbsp;Product Price : <?php echo '<b> $' . $ProductData['PurchsedPrice'] . '</b>'; ?></font></a></h5></td>
                                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                        <td><h5><p><font size="4">&nbsp;&nbsp;Product Info : <?php echo '<b>' . $ProductData['ProductInfo'] . '</b>'; ?></font></p></h5></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                echo '<h5><a style="color:red;margin-left:120px;"><font size="4">Please Select at list one Product</font></a></h5>';
            }
            ?>
            <input type='hidden' name = "payment" id="payment">
            <a class="navbar-brand" style="margin-left: 60px;"><?php echo " Subtotal (" . $totalquantity . " item) : " ?> <strong><?php echo '$' . $totalPrice; ?></strong></a>

            <button class="btn btn-large" style="margin-right: 100px;margin-top: 10px;background-color: red;color: black;">Proceed to Checkout
                <?php echo '(' . $cartCount . ' item)'; ?> 
            </button>
            <br><br><br>
            <!--<a> &nbsp&nbsp&nbsp Note : according to product category service will be execute</a>-->           
            <div class="col-md-12 end-box" style="margin-top: 10%;">
                &copy; 2018 | &nbsp; All Rights Reserved | &nbsp; www.vivek.com | &nbsp; 24x7 support | &nbsp; Email us: info@vivek.com
            </div> 
            <script src="assets/js/jquery-1.10.2.js"></script>
            <script src="assets/js/my.js"></script>
            <script src="assets/js/bootstrap.js"></script>
            <script src="assets/ItemSlider/js/modernizr.custom.63321.js"></script>
            <script src="assets/ItemSlider/js/jquery.catslider.js"></script>
        </form>
    </body>
</html>
