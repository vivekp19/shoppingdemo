<?php
include_once 'HomePage.php';
$homeobj = new HomePage();
$ProductCategorydata = $homeobj->GetProductCategory();
$cartCount = $homeobj->GetProductCartDetails('1');
if (is_array($cartCount) && isset($cartCount[0])) {
    $cartCount = $cartCount[0];
}
?>
<!--Load Default Page-->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Shopping Demo</title>
        <link href="assets/css/bootstrap.css" rel="stylesheet">
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
        <link href="assets/ItemSlider/css/main-style.css" rel="stylesheet" />
        <link href="assets/css/style.css" rel="stylesheet" />
        <link href="assets/css/my.css" rel="stylesheet" />
        <script type="text/javascript">
            function cartAction(action, ProductCode, ProductPrice, showbtn) {
                var queryString = "";
                if (action != "") {
                    switch (action) {
                        case "add":
                            queryString = 'action=' + action + '&pcode=' + ProductCode + '&pprice=' + ProductPrice;
                            break;
                        case "remove":
                            queryString = 'action=' + action + '&pcode=' + ProductCode;
                            break;
                        default :
                            break;

                    }
                    jQuery.ajax({
                        url: "AddCart.php",
                        data: queryString,
                        type: "POST",
                        success: function (data) {
//                            alert(showbtn);
                            $("#cart-item").html(data);
                            switch (action) {
                                case "add":
                                    if (showbtn == '0') {
                                        $("#remove_" + ProductCode).show();
                                    }
                                    break;
//                                case "remove":
//                                    if (showbtn == '1') {
//                                        $("#remove_" + ProductCode).hide();
//                                    }
//                                    break;
                                default :
                                    break;
                            }
                        },
                        error: function () {
                        }
                    });
                }
            }
        </script>
    </head>
    <body>
        <form action="CartPage.php" method="POST" id="addForm" >
            <nav class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><strong>Shopping Demo</strong></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><font size="5">Cart :  <b style="color: red;" id="cart-item"><?php echo $cartCount; ?> </b> </font></a></li>
                       	<li> <button class="btn btn-large btn-success" style="margin-right: 100px;margin-top: 10px;">View Cart</button> </li>
                    </ul>
                </div>
            </nav>
            <div class="container">

                <?php
                foreach ($ProductCategorydata as $value) {
                    $ProductData = $homeobj->GetProductData($value['ProductCategoryId']);
                    echo "<ol class='breadcrumb'>
                            <li><a href='#'>" . $value['ProductCategoryName'] . "</a></li>
                            <li class = 'active'>" . $value['ProductCategoryInfo'] . "</li>
                          </ol><div class='row'>";
                    if (is_array($ProductData)) {
                        foreach ($ProductData as $value1) {
                            $CartaddorNot = $homeobj->GetProductCartaddorNot($value1['ProductId'], '1');
                            $show = isset($CartaddorNot['UserCartInfoID']) && $CartaddorNot['UserCartInfoID'] != '' ? '1' : '0';
                            ?>
                            <div class='col-md-2 text-center col-sm-6 col-xs-6'>
                                <div class='thumbnail product-box'>
                                    <div class='caption'>
                                        <h3><a href='#'><?php echo $value1['ProductName']; ?></a></h3>
                                        <p id="1pprice_<?php echo $value1['ProductId']; ?>"> Product Price : 
                                            <strong> $<?php echo $value1['ProductPrice']; ?> </strong>  </p>
                                        <p> 
                                            <button type="button" style="background-color: #4CAF50;border: none;color: white;padding: 8px 14px 8px 14px;text-align: center;text-decoration: none;display: inline-block;margin: 6px 3px;cursor: pointer;border-radius: 50%;" id = "add_<?php echo $value1['ProductId']; ?>" onClick = 'cartAction("add", "<?php echo $value1['ProductId']; ?>", "<?php echo $value1['ProductPrice']; ?>", "<?php echo $show; ?>")'>
                                                +</button>
                                            <?php if (!empty($CartaddorNot['UserCartInfoID'])) { ?>
                                                <button type="button" style="background-color: red;border: none;color: white;padding: 8px 15px 8px 15px;text-align: center;text-decoration: none;display: inline-block;margin: 6px 3px;cursor: pointer;border-radius: 50%;" id = "remove_<?php echo $value1['ProductId']; ?>" onClick = 'cartAction("remove", "<?php echo $CartaddorNot['UserCartInfoID']; ?>", "<?php echo $value1['ProductPrice']; ?>", "<?php echo $show; ?>")'>
                                                    -</button>
                                            <?php } else { ?>
                                                <button type="button" style="background-color: red;border: none;color: white;padding: 8px 15px 8px 15px;text-align: center;text-decoration: none;display: inline-block;margin: 6px 3px;cursor: pointer;border-radius: 50%;display: none;" id = "remove_<?php echo $value1['ProductId']; ?>" onClick = 'cartAction("remove", "<?php echo $CartaddorNot['UserCartInfoID']; ?>", "<?php echo $value1['ProductPrice']; ?>", "<?php echo $show; ?>")'>
                                                    -</button>
                                            <?php } ?>
                                    </div>
                                </div>
                            </div> <?php
                        }
                    }
                    echo '</div>';
                }
                ?>
            </div>
            <div class="col-md-12 end-box ">
                &copy; 2018 | &nbsp; All Rights Reserved | &nbsp; www.vivek.com | &nbsp; 24x7 support | &nbsp; Email us: info@vivek.com
            </div>
            <script src="assets/js/jquery-1.10.2.js"></script>
            <script src="assets/js/my.js"></script>
            <script src="assets/js/bootstrap.js"></script>
            <script src="assets/ItemSlider/js/modernizr.custom.63321.js"></script>
            <script src="assets/ItemSlider/js/jquery.catslider.js"></script>
        </form>
    </body>
</html>
