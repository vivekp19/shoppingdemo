<?php

class CheckOut {

    public function __construct() {
        $this->SelectPaymentType();
    }

    public function SelectPaymentType() {

        # add static data for payment system
        echo '<html>
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta name="description" content="">
                    <meta name="author" content="">
                    <title>Shopping Demo</title>
                    <link href="assets/css/bootstrap.css" rel="stylesheet">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                    <link href="assets/css/font-awesome.min.css" rel="stylesheet"/>  
                    <link href="assets/ItemSlider/css/main-style.css" rel="stylesheet"/>
                    <link href="assets/css/style.css" rel="stylesheet"/>
                </head>
                <body>
                    <form action="ServiceCheking.php" method="POST" id="addForm" >
                          <nav class="navbar navbar-default" role="navigation">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="index.php"><strong>Shopping Demo</strong></a>
                                <ul class="nav navbar-nav navbar-right">
                                <a class="btn btn-large" href="CartPage.php" style="margin-left: 800px;margin-top: 10px;background-color: white;color: black;"><strong>BACK</strong></a>
                            </div>
                        </nav> 
                        <input type="hidden" name = "payment" id="payment" value="done">
                        <table class="table-bordered" style="margin-left: 30px">
                            <tr> 
                                <td colspan="2" style = "background-color: #4CAF50;"><a class="navbar-brand" style="color:black;"><strong>Enter Payment Details</strong></a> </td> 
                            </tr>
                            <tr>
                                <td><a class="navbar-brand">Card Number </a> </td>
                                <td><a class="navbar-brand"> 1213 1516 1819</a> </td>
                            </tr>
                            <tr> 
                                <td><a class="navbar-brand">Name on card </a> </td>
                                 <td><a class="navbar-brand"> Vivek Patel</a> </td> 
                            </tr>
                            <tr> 
                                <td><a class="navbar-brand">Expiration date </a> </td>
                                 <td><a class="navbar-brand"> 19 JULY 2021</a> </td> 
                            </tr>
                            <tr> 
                                <td><a class="navbar-brand">CVV </a> </td> 
                                 <td><a class="navbar-brand"> 925 </a> </td>
                            </tr>
                        </table>
                        <button class="btn btn-large" style="margin-left: 30px;margin-top: 10px;background-color: red;color: black;">Proceed to Payment </button> 
                    </form>
                </body>
                </html>';
        exit;
    }

}

$obj = new CheckOut();
?>