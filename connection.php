<?php

class Connection {

    private $DB_SERVER = 'localhost';
    private $DB_USERNAME = 'root';
    private $DB_PASSWORD = '';
    private $DB_NAME = 'task';
    protected $connection;

    public function __construct() {
        if (!isset($this->connection)) {
            $this->connection = mysqli_connect($this->DB_SERVER, $this->DB_USERNAME, $this->DB_PASSWORD, $this->DB_NAME);
            if ($this->connection === false) {
                die("ERROR: Could not connect. " . mysqli_connect_error());
            } else {
//                echo 'Database Connected';
            }
        }
        return $this->connection;
    }

}

$connection = new Connection();
?>