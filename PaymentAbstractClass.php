<?php

abstract class PaymentClass {

    public abstract function getPaymentDetails();

    protected abstract function updatePaymentDetails();
}

?>