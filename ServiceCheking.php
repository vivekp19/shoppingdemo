<?php

include_once '/connection.php';

class ServiceCheking extends Connection {

    public function __construct() {
        parent::__construct();
        $this->ServiceUpdate();
    }

    public function ServiceUpdate() {
        $ProductCategorydata = $this->getSelectedCartDetails1(1);
        foreach ($ProductCategorydata as $value1) {
            if ($value1['ProductCategoryId'] != "") {
                switch ($value1['ProductCategoryId']) { #Service checking and as per the service. 
                    case "1": #Physical Item
                        # Generate one shipping label for it to be placed in the shipping box
                        $ServiceDone = $this->generateShipping($value1['ProductCategoryId'], $value1['ProductQuality']);
                        break;
                    case "2": #Service Subscription
                        # Activate the subscription, and notify the user via email about this
                        $ServiceDone = $this->activateSubscription($value1['ProductCategoryId'], $value1['ProductQuality']);
                        break;
                    case "3": #Ordinary Book
                        #generate it shipping label with a notification that it is a tax-exempt item
                        $ServiceDone = $this->generateShippingwithNotification($value1['ProductCategoryId'], $value1['ProductQuality']);
                        break;
                    case "4": #Digital Media 
                        #sending the description of the purchase by email to the buyer, grant a discount voucher of $10 to the buyer associated with the payment.
                        $ServiceDone = $this->sendingMailWithvoucher($value1['ProductCategoryId'], $value1['ProductQuality']);
                        break;
                    default :
                        break;
                }
            }
        }
        #Call Payment API After Service Checking 
        include_once 'PaymentVerify.php';
        $paymentVerifygobj = new PaymentVerify();
        $paymentVerifygobj->getPaymentDetails();
        $paymentVerifygobj->updatePaymentDetails();
        $paymentVerifygobj->PaymentStatusUpdate();
        $this->PaymentSucessfull(1);
        echo '<script>alert("Payment Sucessfully done");'
        . ' window.location.href = "index.php";</script>';
    }

    public function GetSelectedCartDetails1($userID) {
        try {
            $getCardDataSql = "SELECT count(usercartinfo.ProductId) AS 'ProductQuality',productcategory.ProductCategoryId,productcategory.ProductCategoryName,
                                      SUM(usercartinfo.PurchsedPrice) AS 'PurchsedPrice' FROM usercartinfo 
                                    INNER JOIN productlist ON productlist.ProductId = usercartinfo.ProductId
                                    INNER JOIN productcategory ON productcategory.ProductCategoryId = productlist.ProductCategoryId 
                                       WHERE usercartinfo.UserID = " . $userID . " GROUP BY productcategory.ProductCategoryId";
            if ($resultdata = mysqli_query($this->connection, $getCardDataSql)) {
                while ($row = mysqli_fetch_assoc($resultdata)) {
                    $data[] = $row;
                }
                return $data;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function generateShipping() {
        #Generate shipping label and placed in the shipping box
    }

    public function activateSubscription() {
        #Activate the subscription
        #Sending email
    }

    public function generateShippingwithNotification() {
        #Generate shipping label
        #Notification that it is a tax-exempt item
    }

    public function sendingMailWithvoucher() {
        #sending Mail rant a discount voucher of $10 to the buyer associated 
    }

    public function PaymentSucessfull($userID) {
        $delete_query = "DELETE FROM usercartinfo WHERE UserID = '" . $userID . "'";
        if (mysqli_query($this->connection, $delete_query)) {
            return true;
        }
    }

}

$homobj = new ServiceCheking();
?>