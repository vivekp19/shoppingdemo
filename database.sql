-- Data Base --

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task`
--
CREATE DATABASE IF NOT EXISTS `task` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `task`;

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE `productcategory` (
  `ProductCategoryId` int(4) NOT NULL,
  `ProductCategoryName` varchar(50) DEFAULT NULL,
  `ProductCategoryInfo` varchar(100) DEFAULT NULL,
  `IsActive` enum('0','1') NOT NULL DEFAULT '0',
  `Allowtax` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productcategory`
--

INSERT INTO `productcategory` (`ProductCategoryId`, `ProductCategoryName`, `ProductCategoryInfo`, `IsActive`, `Allowtax`) VALUES
(1, 'Physical Item', 'Generate one shipping label in the shipping box', '0', '0'),
(2, 'Service Subscription', 'Activate the subscription and notify the user via email about this', '0', '0'),
(3, 'Ordinary Book', 'Generate it shipping label - with a notification that it is a tax-exempt item ', '0', '1'),
(4, 'Digital Media', 'Sending the description of the purchase by email to the buyer', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `productlist`
--

CREATE TABLE `productlist` (
  `ProductId` int(4) NOT NULL,
  `ProductCategoryId` int(4) DEFAULT NULL,
  `ProductName` varchar(100) DEFAULT NULL,
  `ProductPrice` int(4) DEFAULT NULL,
  `ProductInfo` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productlist`
--

INSERT INTO `productlist` (`ProductId`, `ProductCategoryId`, `ProductName`, `ProductPrice`, `ProductInfo`) VALUES
(14, 3, 'Product 303', 200, 'Product 303 - Ordinary Book'),
(12, 2, 'Product 203', 600, 'Product 203 - Service Subscription'),
(9, 1, 'Product 102', 100, 'Product 102 - Physical Item'),
(8, 4, 'Product 401', 300, 'Product 401 - Ordinary Book'),
(7, 4, 'Product 400', 600, 'Product 400 - Ordinary Book'),
(6, 3, 'Product 301', 200, 'Product 301 - Ordinary Book'),
(5, 3, 'Product 300', 500, 'Product 300 - Ordinary Book'),
(4, 2, 'Product 201', 600, 'Product 201 - Service Subscription'),
(3, 2, 'Product 200', 300, 'Product 200 - Service Subscription'),
(2, 1, 'Product 101', 200, 'Product 101 - Physical Item'),
(1, 1, 'Product 100', 100, 'Product 100 - Physical Item'),
(13, 3, 'Product 302', 500, 'Product 302 - Ordinary Book'),
(11, 2, 'Product 202', 300, 'Product 202 - Service Subscription'),
(10, 1, 'Product 103', 200, 'Product 103 - Physical Item'),
(15, 4, 'Product 402', 600, 'Product 402 - Digital Media'),
(16, 4, 'Product 403', 300, 'Product 403 - Digital Media');

-- --------------------------------------------------------

--
-- Table structure for table `usercartinfo`
--

CREATE TABLE `usercartinfo` (
  `UserCartInfoID` int(4) NOT NULL,
  `UserID` int(4) NOT NULL,
  `ProductId` int(4) DEFAULT NULL,
  `PurchsedPrice` int(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `UserID` int(4) NOT NULL,
  `UserName` varchar(50) NOT NULL,
  `City` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`UserID`, `UserName`, `City`) VALUES
(1, 'vivek', 'Ahmedabad');

-- --------------------------------------------------------

--
-- Table structure for table `userpaymentinfo`
--

CREATE TABLE `userpaymentinfo` (
  `UserPaymentInfoID` int(4) NOT NULL,
  `UserID` int(4) DEFAULT NULL,
  `TotalPaymentAmount` int(4) DEFAULT NULL,
  `TotalProductQuality` int(4) DEFAULT NULL,
  `PaymentType` enum('0','1') DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `productcategory`
--
ALTER TABLE `productcategory`
  ADD PRIMARY KEY (`ProductCategoryId`);

--
-- Indexes for table `productlist`
--
ALTER TABLE `productlist`
  ADD PRIMARY KEY (`ProductId`),
  ADD KEY `ProductCategoryId_fk` (`ProductCategoryId`);

--
-- Indexes for table `usercartinfo`
--
ALTER TABLE `usercartinfo`
  ADD PRIMARY KEY (`UserCartInfoID`),
  ADD KEY `ProductId_fbk` (`ProductId`),
  ADD KEY `UserID_fbk` (`UserID`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`UserID`);

--
-- Indexes for table `userpaymentinfo`
--
ALTER TABLE `userpaymentinfo`
  ADD PRIMARY KEY (`UserPaymentInfoID`),
  ADD KEY `UserCartInfoID_fk` (`UserID`),
  ADD KEY `UserID_fk` (`UserID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `productcategory`
--
ALTER TABLE `productcategory`
  MODIFY `ProductCategoryId` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `productlist`
--
ALTER TABLE `productlist`
  MODIFY `ProductId` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `usercartinfo`
--
ALTER TABLE `usercartinfo`
  MODIFY `UserCartInfoID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `UserID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `userpaymentinfo`
--
ALTER TABLE `userpaymentinfo`
  MODIFY `UserPaymentInfoID` int(4) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
