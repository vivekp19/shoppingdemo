<?php

include_once 'HomePage.php';

class AddCart {

    public function __construct() {
        $homeobj = new HomePage();
        if ($_POST['action'] != "") {
            switch ($_POST['action']) {
                case "add":
                    $cartCount = $homeobj->addCart($_POST['pcode'], $_POST['pprice']);
                    echo $cartCount[0];
                    break;
                case "remove":
                    $cartCount = $homeobj->removeCart($_POST['pcode']);
                    echo $cartCount[0];
                    break;
                default :
                    break;
            }
        }
    }

}

$obj = new AddCart();
?>