<?php

#include abstract PaymentClass
include_once '/PaymentAbstractClass.php';

class PaymentVerify extends PaymentClass {
    # According to Payment gatway use Payment Api and Get data from APi

    public function getPaymentDetails() { #abstract Class
        #get the Payment Details From Payment API Like Paypal
    }

    public function updatePaymentDetails() {#abstract Class
        # update Payment Details of User
    }

    public function PaymentStatusUpdate() { #extra Class 
        # Update Payment Status and Confirm the payment status to user
    }

}

$PaymentVerifyobj = new PaymentVerify();
?>