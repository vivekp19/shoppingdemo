<?php

#Load connction file for database connction
include_once '/connection.php';

class HomePage extends Connection {

    public function __construct() {
//        Call connction class construct
        parent::__construct();
    }

    public function GetProductCategory() {
        try {
            $sql = "SELECT * FROM productcategory";
            if ($result = mysqli_query($this->connection, $sql)) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $data[] = $row;
                }
                return $data;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function GetProductData($ProductCategoryId) {
        try {
            $sql = "SELECT * FROM productlist "
                    . " INNER JOIN productcategory "
                    . "  ON productlist.ProductCategoryId = productcategory.ProductCategoryId"
                    . " WHERE productlist.ProductCategoryId = " . $ProductCategoryId . " ";
            if ($result = mysqli_query($this->connection, $sql)) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $data[] = $row;
                }
                return $data;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function addCart($ProductId, $PurchsedPrice) {
        try {
            $insert_query = "INSERT INTO usercartinfo(UserID, ProductId,PurchsedPrice) VALUES ('1','" . $ProductId . "','" . $PurchsedPrice . "')";
            if (mysqli_query($this->connection, $insert_query)) {
                $data = $this->GetProductCartDetails(1);
                return $data;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function removeCart($UserCartInfoID) {
        try {
            $insert_query = "DELETE FROM usercartinfo WHERE UserCartInfoID = '" . $UserCartInfoID . "' AND UserID = '1'";
            if (mysqli_query($this->connection, $insert_query)) {
                $data = $this->GetProductCartDetails(1);
                return $data;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function GetProductCartDetails($UserID) {
        try {
            $sql = "SELECT COUNT(*) FROM usercartinfo WHERE usercartinfo.UserID = " . $UserID . " ";
            if ($result = mysqli_query($this->connection, $sql)) {
                return mysqli_fetch_row($result);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function GetSelectedCartDetails($UserID) {
        try {
            $sql = "SELECT productcategory.ProductCategoryId,productcategory.ProductCategoryName,productlist.ProductId,
                           usercartinfo.PurchsedPrice,productlist.ProductInfo
                    FROM productcategory 
                        INNER JOIN productlist ON productlist.ProductCategoryId = productcategory.ProductCategoryId 
                        INNER JOIN usercartinfo ON usercartinfo.ProductId = productlist.ProductId
                    WHERE usercartinfo.UserID = '" . $UserID . "'";
            $data = array();
            if ($result = mysqli_query($this->connection, $sql)) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $data[] = $row;
                }
                return $data;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    public function GetProductCartaddorNot($ProductId, $UserID) {
        try {
            $sql = "SELECT usercartinfo.UserCartInfoID FROM usercartinfo WHERE usercartinfo.ProductId = " . $ProductId . " AND usercartinfo.UserID = " . $UserID . " LIMIT 0,1";
            if ($result = mysqli_query($this->connection, $sql)) {
                return mysqli_fetch_assoc($result);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

}

$homobj = new HomePage();
?>